import os
import sys

import pygame

FPS = 50


def load_image(name):
    fullname = os.path.join('data', name)
    if not os.path.isfile(fullname):
        print(f"Файл с изображением '{fullname}' не найден")
        sys.exit()
    image = pygame.image.load(fullname)
    return image


def terminate():
    pygame.quit()
    sys.exit()


def start_screen():
    pygame.init()
    intro_text = ["Описание игры",
                  "игра для двух игроков",
                  "в ней всего 3 уровня",
                  "цель - попасть в противника",
                  "кто больше одержит побед в 3-х раундов, тот выйграл",
                  "1 игрок управляет с помощью кнопок 'a', 'w', 'd', 's' и стреляет с помощью кнопки 'space'",
                  "2 игрок управляет с помощью стрелок и стреляет с помощью кнопки 'enter'"]

    fon = pygame.transform.scale(load_image('fon.jpg'), (1500, 900))
    screen.blit(fon, (0, 0))
    font = pygame.font.Font(None, 45)
    text_coord = 10
    for line in intro_text:
        string_rendered = font.render(line, True, pygame.Color('green'))
        intro_rect = string_rendered.get_rect()
        text_coord += 10
        intro_rect.top = text_coord
        intro_rect.x = 10
        text_coord += intro_rect.height
        screen.blit(string_rendered, intro_rect)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                terminate()
            elif event.type == pygame.KEYDOWN or \
                    event.type == pygame.MOUSEBUTTONDOWN:
                return
        pygame.display.flip()
        clock.tick(FPS)


class Border(pygame.sprite.Sprite):
    def __init__(self, x1, y1, x2, y2):
        super().__init__(all_sprites)
        if x1 == x2:  # вертикальная стенка
            self.add(vertical_borders)
            self.image = pygame.Surface([1, y2 - y1])
            self.image.fill((255, 0, 0))
            self.rect = pygame.Rect(x1, y1, 1, y2 - y1)
        else:  # горизонтальная стенка
            self.add(horizontal_borders)
            self.image = pygame.Surface([x2 - x1, 1])
            self.image.fill((255, 0, 0))
            self.rect = pygame.Rect(x1, y1, x2 - x1, 1)


class MoveTanks1(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__(all_sprites)
        self.image = pygame.Surface((60, 60),
                                    pygame.SRCALPHA, 32)
        pygame.draw.rect(self.image, (0, 0, 255),
                         (x, y, 60, 60))
        self.rect = pygame.Rect(x, y, 30, 30)
        self.x, self.y = x, y

    def image(self):
        pygame.draw.rect(screen, (0, 0, 255),
                         (self.x, self.y, 60, 60))

    def update(self, keys_pressed):
        if keys_pressed[pygame.K_a]:
            self.x -= 10
        if keys_pressed[pygame.K_d]:
            self.x += 10
        if keys_pressed[pygame.K_w]:
            self.y -= 10
        if keys_pressed[pygame.K_s]:
            self.y += 10
        if pygame.sprite.spritecollideany(self, horizontal_borders):
            self.y = 0
        if pygame.sprite.spritecollideany(self, vertical_borders):
            self.x = 0


class MoveTanks2:
    pass


size = width, height = 1500, 900
screen = pygame.display.set_mode(size)

all_sprites = pygame.sprite.Group()
horizontal_borders = pygame.sprite.Group()
vertical_borders = pygame.sprite.Group()

pygame.display.set_caption('Танчики')
clock = pygame.time.Clock()
start_screen()

running = True
tank1 = MoveTanks1(100, 400)
Border(5, 5, width - 5, 5)
Border(5, height - 5, width - 5, height - 5)
Border(5, 5, 5, height - 5)
Border(width - 5, 5, width - 5, height - 5)
tank2 = MoveTanks2()

pygame.display.flip()
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminate()
    screen.fill((0, 0, 0))
    keys_pressed = pygame.key.get_pressed()
    all_sprites.draw(screen)
    all_sprites.update(keys_pressed)
    pygame.display.flip()
    clock.tick(FPS)

print("ffffff")
print("ф")

